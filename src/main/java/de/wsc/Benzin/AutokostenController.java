package de.wsc.Benzin;

import de.wsc.Benzin.db.AutokostenEntity;
import de.wsc.Benzin.db.AutokostenEntityRepository;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Map;

@Controller
public class AutokostenController {

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(BigDecimal.class, new BigDecimalEditor());
    }

    private final AutokostenEntityRepository autokostenEntityRepository;

    public AutokostenController(AutokostenEntityRepository repository) {
        autokostenEntityRepository = repository;
    }

    @GetMapping("/auto")
    public String get(Map<String, Object> model, HttpSession httpSession) {
        model.put("lastSavedId", httpSession.getAttribute("lastSavedId"));
        fillEntities(model);
        return "autokosten";
    }

    private void fillEntities(Map<String, Object> model) {
        Iterable<AutokostenEntity> autokosten = autokostenEntityRepository.findAllByOrderByDatumDesc();
        model.put("entities", autokosten);
        model.put("autokostenEntity", getAutokostenEntity());
        model.put("gesamt", getGesamt(autokosten));
    }

    private Gesamt getGesamt(Iterable<AutokostenEntity> autokosten) {
        Gesamt gesamt = new Gesamt();
        AutokostenEntity first = null;
        AutokostenEntity last = null;
        for (AutokostenEntity kosten : autokosten) {
            if (first == null) {
                first = kosten;
            }
            last = kosten;
            gesamt.addBetrag(kosten.getBetrag());
            gesamt.addLiter(kosten.getLiter());
            gesamt.addDurchschnitt(kosten.getDurchschnitt());
            gesamt.addTageskilometer(kosten.getTageskilometer());
        }
        if (first != null) {
            gesamt.setKilometer(first.getGesamtkilometer() - last.getGesamtkilometer());
        }

        return gesamt;
    }

    @PostMapping("/auto")
    public RedirectView post(@ModelAttribute("autokostenEntity") AutokostenEntity entity, HttpSession httpSession) {
        entity =  autokostenEntityRepository.save(entity);
        httpSession.setAttribute("lastSavedId", entity.getId());
        RedirectView redirectView = new RedirectView();
        redirectView.setUrl("/auto");
        return redirectView;
    }

    @PostMapping("/delete")
    public RedirectView  delete(@RequestParam("id") Long id) {
        autokostenEntityRepository.deleteById(id);
        RedirectView redirectView = new RedirectView();
        redirectView.setUrl("/auto");
        return redirectView;
    }

    private AutokostenEntity getAutokostenEntity() {
        AutokostenEntity autokostenEntity = new AutokostenEntity();
        autokostenEntity.setBeschreibung("Nürnberg");
        autokostenEntity.setBetrag(BigDecimal.ZERO);
        autokostenEntity.setDatum(LocalDate.now());
        autokostenEntity.setGesamtkilometer(0);
        autokostenEntity.setTageskilometer(0);
        autokostenEntity.setLiter(BigDecimal.ZERO);
        autokostenEntity.setTags("");
        return autokostenEntity;
    }
}
