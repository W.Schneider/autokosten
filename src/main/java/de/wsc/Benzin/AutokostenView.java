package de.wsc.Benzin;

import de.wsc.Benzin.db.AutokostenEntity;

public class AutokostenView {
    private Iterable<AutokostenEntity> entities;

    public AutokostenView() {}

    public AutokostenView(Iterable<AutokostenEntity> entities) {
        this.entities = entities;
    }

    public Iterable<AutokostenEntity> getEntities() {
        return entities;
    }

    public void setEntities(Iterable<AutokostenEntity> entities) {
        this.entities = entities;
    }
}
