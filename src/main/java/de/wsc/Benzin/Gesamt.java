package de.wsc.Benzin;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Gesamt {
    private BigDecimal betrag = BigDecimal.ZERO;
    private int kilometer = 0;
    private int tageskilometer = 0;
    private BigDecimal liter = BigDecimal.ZERO;
    private BigDecimal durchschnitt = BigDecimal.ZERO;
    private int count = 0;

    public void addDurchschnitt(BigDecimal liter) {
        count++;
        durchschnitt = durchschnitt.add(liter);
    }

    public BigDecimal getDurchschnitt() {
        return durchschnitt.divide(BigDecimal.valueOf(count), RoundingMode.HALF_DOWN);
    }

    public BigDecimal getBetrag() {
        return betrag;
    }

    public void addBetrag(BigDecimal amount) {
        if (amount != null) {
            betrag = betrag.add(amount);
        }
    }

    public int getKilometer() {
        return kilometer;
    }

    public void setKilometer(int kilometer) {
        this.kilometer = kilometer;
    }

    public BigDecimal getLiter() {
        return liter;
    }

    public void addLiter(BigDecimal amount) {
        liter = liter.add(amount);
    }

    public int getTageskilometer() {
        return tageskilometer;
    }

    public void addTageskilometer(int tageskilometer) {
        this.tageskilometer += tageskilometer;
    }
}
