package de.wsc.Benzin.db;


// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

import org.springframework.data.repository.CrudRepository;

public interface AutokostenEntityRepository extends CrudRepository<AutokostenEntity, Long> {
    Iterable<AutokostenEntity> findAllByOrderByGesamtkilometerDesc();
    Iterable<AutokostenEntity> findAllByOrderByDatumDesc();

}
