<!DOCTYPE html>
<html lang="de">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Autokosten</title>

    <style>

        thead {
            background-color: chartreuse;
        }

        tbody tr:nth-child(odd) {
            background-color: #ccc;
        }

        tbody tr.selected {
            background-color: aquamarine;
        }

        tbody tr.lastSaved {
            background-color: greenyellow;
        }

        tbody tr:hover {
            background-color: bisque;
            cursor: pointer;
        }
        tbody tr input {
            width: 100%;
            text-align: right;
        }

        tbody tr input#datum {
            width: 100%;
            text-align: center;
        }

        tbody tr input#beschreibung {
            width: 100%;
            text-align: left;
        }

        td[data-datum] {
            text-align: center;
        }

        td[data-betrag] {
            text-align: right;
        }

        td[data-tageskilometer] {
            text-align: right;
        }

        td[data-gesamtkilometer] {
            text-align: right;
        }

        td[data-liter] {
            text-align: right;
        }

        td[data-durchschnitt] {
            text-align: right;
        }

        td[data-beschreibung] {
            text-align: left;
        }


        /* highlight mouse over column
        table {
            overflow: hidden;
            z-index: 1;
        }

        td, th {
            position: relative;
        }

        td:hover::after {
            background-color: bisque;
            content: '';
            height: 10000px;
            left: 0;
            position: absolute;
            top: -5000px;
            width: 100%;
            z-index: -1;
        }
         */

    </style>

    <script>
        document.addEventListener("DOMContentLoaded", function(event){
            function deleteKosten(evnt) {
                let id = evnt.target.value;
                let deleteForm = document.querySelector("#deleteForm");
                let deleteId = deleteForm.querySelector("input");
                deleteId.value = id;
                deleteForm.submit();
            }

            function selectRow(evnt) {
                let id = evnt.currentTarget.dataset['id'];
                let row = document.querySelector("[data-id='" + id + "']");
                document.querySelector("input[name=id]").value = id;
                copyValue("datum", row);
                copyValue("betrag", row);
                copyValue("tageskilometer", row);
                copyValue("gesamtkilometer", row);
                copyValue("liter", row);
                copyValue("beschreibung", row);

                removeClass("selected");
                row.classList.add("selected");
            }

            function removeClass(cls) {
                let elements = document.querySelectorAll("[class=selected]");
                for (let i = 0; i <elements.length; i++) {
                    elements[i].classList.remove(cls);
                }
            }

            function copyValue(key, row) {
                document.querySelector("input[name=" + key + "]").value = row.querySelector("[data-" + key + "]").innerHTML;
            }

            function create() {
                let saveForm = document.querySelector("form[name='saveForm']");
                let id = document.querySelector("input[name='id']");
                id.value = "";
                saveForm.submit();
            }

            function adjustNumbers() {
                let element = document.querySelector("input#tageskilometer");
                element.value = element.value.replace(".", "");
                element = document.querySelector("input#gesamtkilometer");
                element.value = element.value.replace(".", "");
            }

            function durchschnitt() {
                let liter = document.getElementById("liter").value;
                let tageskilometer = document.getElementById("tageskilometer").value;
                liter.replaceAll(",", ".");
                if (liter > 0 && tageskilometer > 0) {
                    document.getElementById("durchschnitt").innerText = liter * 100 / tageskilometer;
                    // https://stackoverflow.com/questions/3084675/how-does-internationalization-work-in-javascript
                }
            }

            function wire() {
                let i;
                let buttons = document.querySelectorAll("button[name=delete]");
                for (i = 0; i <buttons.length; i++) {
                    buttons[i].addEventListener("click", deleteKosten)
                }

                document.querySelector("button[name=create]").addEventListener("click", create);

                let elements = document.querySelectorAll("tr[data-id]");
                for (i = 0; i <buttons.length; i++) {
                    elements[i].addEventListener("click", selectRow)
                }

                document.querySelector("form[name=saveForm]").addEventListener("submit", adjustNumbers);
                document.getElementById("tageskilometer").addEventListener("focusout", durchschnitt);
                document.getElementById("liter").addEventListener("focusout", durchschnitt);
            }
            wire();
            let datumElement = document.getElementById("datum");
            datumElement.focus();
            datumElement.select();
        });
    </script>
</head>
<fmt:setLocale value="de_DE" scope="session"/>
<body>
    <table>
        <thead>
            <tr>
                <th><label for="datum">Datum</label></th>
                <th><label for="betrag">Betrag</label></th>
                <th><label for="tageskilometer">Tageskilometer</label></th>
                <th><label for="gesamtkilometer">Gesamtkilometer</label></th>
                <th><label for="liter">Liter</label></th>
                <th>Durchschnitt</th>
                <th><label for="beschreibung">Beschreibung</label></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td data-beschreibung>Gesamt</td>
                <td data-betrag><fmt:formatNumber currencySymbol="€" value="${gesamt.betrag}" pattern="#,##0.00 €"/></td>
                <td data-tageskilometer><fmt:formatNumber value="${gesamt.tageskilometer}" pattern="#,##0 km"/></td>
                <td data-gesamtkilometer><fmt:formatNumber value="${gesamt.kilometer}" pattern="#,##0 km"/></td>
                <td data-liter><fmt:formatNumber value="${gesamt.liter}" pattern="#,##0.00 L"/></td>
                <td data-durchschnitt><fmt:formatNumber value="${gesamt.durchschnitt}" pattern="#,##0.00 L"/></td>
                <td></td>
            </tr>
            <tr>
            <form:form name="saveForm" action="auto" method="post" modelAttribute="autokostenEntity">
                <fmt:parseDate  value="${autokostenEntity.datum}"  type="date" pattern="yyyy-MM-dd" var="parsedDate" />
                <fmt:formatDate value="${parsedDate}" type="date" pattern="dd.MM.yyyy" var="stdDatum" />
                <input type="hidden" name="id" value="${autokostenEntity.id}"/>
                <td><input id="datum" type="text" name="datum" value="${stdDatum}"/></td>
                <td><input id="betrag" type="text" name="betrag" value="<fmt:formatNumber value="${autokostenEntity.betrag}" pattern='#,##0.00'/>"/></td>
                <td><input id="tageskilometer" type="text" name="tageskilometer" value="${autokostenEntity.tageskilometer}"/></td>
                <td><input id="gesamtkilometer" type="text" name="gesamtkilometer" value="${autokostenEntity.gesamtkilometer}"/></td>
                <td><input id="liter" type="text" name="liter" value="${autokostenEntity.liter}"/></td>
                <td id="durchschnitt"></td>
                <td><input id="beschreibung" type="text" name="beschreibung" value="${autokostenEntity.beschreibung}"/></td>
                <td>
                    <button name="create" type="submit">create</button>
                    <button name="change" type="submit">change</button>
                </td>
            </form:form>
            </tr>
            <c:forEach items="${entities}" var="autokosten">
                <fmt:parseDate  value="${autokosten.datum}"  type="date" pattern="yyyy-MM-dd" var="parsedDate" />
                <fmt:formatDate value="${parsedDate}" type="date" pattern="dd.MM.yyyy" var="stdDatum" />
            <tr data-id="${autokosten.id}" class="${lastSavedId == autokosten.id ? 'lastSaved' : ''}">
                <td data-datum>${stdDatum}</td>
                <td data-betrag><fmt:formatNumber value="${autokosten.betrag}" pattern="#,##0.00"/></td>
                <td data-tageskilometer><fmt:formatNumber value="${autokosten.tageskilometer}" pattern="#,##0"/></td>
                <td data-gesamtkilometer><fmt:formatNumber value="${autokosten.gesamtkilometer}" pattern="#,##0"/></td>
                <td data-liter><fmt:formatNumber value="${autokosten.liter}" pattern="#,##0.00"/></td>
                <td data-durchschnitt><fmt:formatNumber value="${autokosten.durchschnitt}" pattern="#,##0.00"/></td>
                <td data-beschreibung>${autokosten.beschreibung}</td>
                <td>
                    <button name="delete" type="submit" value="${autokosten.id}">delete</button>
                </td>

            </tr>
            </c:forEach>
        </tbody>
    </table>

<form id="deleteForm" action="delete" method="post">
    <input type="hidden" name="id" />
</form>

</body>

</html>
